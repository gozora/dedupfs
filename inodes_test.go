package dedupfs

import (
	"testing"
)

var (
	masterNdx = MasterIndex{
		"/1/2/3/4/5/6/7/8/9": {Link: ""},
		"/f6":                {Link: "1/2/3/4/5/6"},
		"/a/b/file.txt":      {Link: ""},
		"/1/2/3/4/l5":        {Link: "5"},
		"/1/2/3/4/l4":        {Link: "../4"},
		"/1/2/3/4/l1":        {Link: "/a/b"},
		"/f8":                {Link: "/f6/7"},
		"/1/2/f2":            {Link: "../../f6"},
		"/a/b/c/rootA":       {Link: "/"},
		"/1/f1.lnk":          {Link: "../a/b/file.txt"},
		"/a/b/c/rootR":       {Link: "../../../"},
		"/file.txt":          {Link: ""},
		"/a/b/c/lf2":         {Link: "/1/2/f2/7/8/.."},
		"/a/b/c/d/e/f":       {Link: ""},
		"/a/l1":              {Link: "/t1"},
		"/t1":                {Link: "f8"},
		"/max/m1":            {Link: "m2"},
		"/max/m2":            {Link: "m3"},
		"/max/m3":            {Link: "m4"},
		"/max/mLast":         {Link: ""},
		"/max/m4":            {Link: "m5"},
		"/max/m5":            {Link: "m6"},
		"/max/m6":            {Link: "m7"},
		"/max/m7":            {Link: "m8"},
		"/max/m8":            {Link: "m9"},
		"/max/m9":            {Link: "mLast"},
		"/max/c1":            {Link: "c2"},
		"/max/c2":            {Link: "c3"},
		"/max/c3":            {Link: "c1"},
	}
)

func TestCreateInodes(t *testing.T) {
	testSet := []struct {
		path string
		want uint
		err  bool
	}{
		{"/1/2/3/4/5/6/7", DirectoryType, false},
		{"/1/2/3/4", DirectoryType, false},
		{"/a/b/c/rootA", SymLinkType, false},
		{"/a/b/c/rootR", SymLinkType, false},
		{"/1/2/3/4/5/6/7/8/9", FileType, false},
		{"/1/2/f2", SymLinkType, false},
		{"/f8", SymLinkType, false},
		{"/file.txt", FileType, false},
		{"/not_existing", FileType, true},
	}

	root := DentryT{
		name:  "/",
		eType: DirectoryType,
	}
	root.root = &root
	CreateInodes(masterNdx, &root)

	for _, e := range testSet {
		de, err := root.getDentry(e.path, false)

		if e.err == false {
			if err != nil {
				t.Fatal(err)
			}

			if de.eType != e.want {
				t.Fatalf(`GetDentry(%q) failed want %v get %v`, e.path, e.want,
					de.eType)
			}
		} else {
			if err == nil {
				t.Fatalf(`GetDentry(%q): This should be an error`, e.path)
			}
		}
	}
}

func TestResolveLinkInodes(t *testing.T) {
	testSet := []struct {
		in   string
		want string
		err  bool
	}{
		{"/1/2/3", "/1/2/3", false},
		{"/f6", "/1/2/3/4/5/6", false},
		{"/", "/", false},
		{"/1/2/3/4/l5/6/7", "/1/2/3/4/5/6/7", false},
		{"/1/2/3/4/l4/5/6/7", "/1/2/3/4/5/6/7", false},
		{"/1/2/3/4/l1/c/d/e/f", "/a/b/c/d/e/f", false},
		{"/1/2/3/4/l1/c/d/../d/e/f", "/a/b/c/d/e/f", false},
		{"/1/2/3/4/l1/c/d/./../d/e/f", "/a/b/c/d/e/f", false},
		{"/a/b/c/rootA/file.txt", "/file.txt", false},
		{"/a/b/c/rootR/file.txt", "/file.txt", false},
		{"/a/b/c/rootA", "/", false},
		{"/a/b/c/rootR", "/", false},
		{"/1/2/3/4/l5/6/7/8/9", "/1/2/3/4/5/6/7/8/9", false},
		{"/1/2/3/4/5/6/7/8/9", "/1/2/3/4/5/6/7/8/9", false},
		{"/1/f1.lnk", "/a/b/file.txt", false},
		{"/1/2/f2/7", "/1/2/3/4/5/6/7", false},
		{"/f8", "/1/2/3/4/5/6/7", false},
		{"/a/b/c/lf2/../../c/lf2/8", "/1/2/3/4/5/6/7/8", false},
		{"/t1", "/1/2/3/4/5/6/7", false},
		{"/missing", "", true},
	}

	root := DentryT{
		name:  "/",
		eType: DirectoryType,
	}
	root.root = &root

	CreateInodes(masterNdx, &root)

	for _, e := range testSet {
		out, err := root.resolveLink(e.in, false, MaxLinkDepth, nil)
		if err != nil && e.err == false {
			t.Fatal(err)
		}

		// Test might NOT fail
		if e.err == false {
			if e.want != out {
				t.Fatalf(`FollowLink(%q) failed want %v get %v`, e.in, e.want, out)
			}
			// Test MUST fail
		} else {
			if err == nil {
				t.Fatalf(`FollowLink(%q): This should be an error`, e.in)
			}
		}
	}
}

func TestCacheInodes(t *testing.T) {
	testSet := []struct {
		in   string
		want string
		err  bool
	}{
		{"/f6", "/1/2/3/4/5/6", false},
		{"/1/f1.lnk", "/a/b/file.txt", false},
		{"/1/2/f2", "/1/2/3/4/5/6", false},
		{"/f8", "/1/2/3/4/5/6/7", false},
		{"/t1", "/1/2/3/4/5/6/7", false},
		{"/a/l1/8", "/1/2/3/4/5/6/7/8", false},
		{"/1/2/3", "/1/2/3", false},
		{"/a/b/c/rootA", "/", false},
		{"/a/b/c/rootR", "/", false},
		{"/1/2/f2/7", "/1/2/3/4/5/6/7", false},
		{"/missing", "", true},
	}

	root := DentryT{
		name:  "/",
		eType: DirectoryType,
	}
	root.root = &root

	CreateInodes(masterNdx, &root)
	// root.CreateLinksCache()

	for _, e := range testSet {
		de, err := root.getDentry(e.in, true)
		if err != nil && e.err == false {
			t.Fatal(err)
		}

		if e.err == false {
			out := de.fullPath()

			if e.want != out {
				t.Logf(`Testing cache for %q failed, want %v get %q`,
					e.in, e.want, out)
				t.Fail()
			}
		} else {
			if err == nil {
				t.Fatalf(`Testing cache for %q: This should be an error`, e.in)
			}
		}
	}
}

func TestUpdateCacheInodes(t *testing.T) {
	root := DentryT{
		name:  "/",
		eType: DirectoryType,
	}
	root.root = &root

	testSet := []struct {
		in   string
		want string
		err  bool
	}{
		// !!! LINKS ONLY !!!
		{"/f6", "/1/2/3/4/5/6", false},
		{"/1/f1.lnk", "/a/b/file.txt", false},
		{"/1/2/f2", "/1/2/3/4/5/6", false},
		{"/f8", "/1/2/3/4/5/6/7", false},
		{"/t1", "/1/2/3/4/5/6/7", false},
		{"/a/b/c/rootA", "/", false},
		{"/a/b/c/rootR", "/", false},
		{"/1/2/f2", "/1/2/3/4/5/6", false},
		{"/missing", "", true},
	}

	CreateInodes(masterNdx, &root)

	for _, e := range testSet {
		de, err := root.getDentry(e.in, false)
		if err != nil && e.err == false {
			t.Fatal(err)
		}

		if e.err == false {
			de.link.dst = de
			de.updateSingleLinkCache()

			out := de.link.dst.fullPath()

			if e.want != out {
				t.Logf(`Testing cache for %q failed, want %v get %q`,
					e.in, e.want, out)
				t.Fail()
			}
		} else {
			if err == nil {
				t.Fatalf(`Testing cache for %q: This should be an error`, e.in)
			}
		}
	}
}

func TestMaxLinkDepthInodes(t *testing.T) {
	testSet := []struct {
		in   string
		want string
	}{
		{"/max/m1", "/max/m1"},    // Link is too deep, it will point to self
		{"/max/m2", "/max/m2"},    // Link is too deep, it will point to self
		{"/max/m3", "/max/mLast"}, // Link is correct
		{"/max/m9", "/max/mLast"}, // Link is correct
	}

	root := DentryT{
		name:  "/",
		eType: DirectoryType,
	}
	root.root = &root

	CreateInodes(masterNdx, &root)
	// root.CreateLinksCache()

	for _, e := range testSet {
		de, err := root.getDentry(e.in, true)
		if err != nil {
			t.Fatal(err)
		}

		out := de.fullPath()

		if out != e.want {
			t.Logf(`Testing link depth for %q failed, want %v get %q`,
				e.in, e.want, out)
			t.Fail()
		}

	}
}

func TestLinkCycleInodes(t *testing.T) {
	testSet := []struct {
		in     string
		depth  int
		isLoop bool
	}{
		{"/max/c1", MaxLinkDepth, true},
		{"/f6", MaxLinkDepth, false},
	}

	root := DentryT{
		name:  "/",
		eType: DirectoryType,
	}
	root.root = &root

	CreateInodes(masterNdx, &root)

	for _, e := range testSet {
		MaxLinkDepth = e.depth
		de, _ := root.getDentry(e.in, false)

		err := de.updateSingleLinkCache()
		if e.isLoop {
			if err == nil || err.err != errDentryMaxLink {
				t.Logf(`Failed to detect loop in : %q`, e.in)
				t.Fail()
			}
		}
	}
}
