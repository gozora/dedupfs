package header

import (
	"encoding/binary"
	"os"
)

func Read(headerFd *os.File) (*Header, error) {
	var header = new(Header)

	// Read actual header.
	err := binary.Read(headerFd, binary.LittleEndian, header)
	if err != nil {
		return nil, err
	}

	return header, nil
}
