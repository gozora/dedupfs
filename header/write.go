package header

import (
	"encoding/binary"
	"errors"
	"os"
)

func Write(headerFd *os.File, blockSz, cnt, blkDedup, blkTotal, ndxPos,
	ndxSz int64) error {
	h := Header{
		BlockSz:    blockSz,
		FilesCnt:   cnt,
		BlkDeduped: blkDedup,
		BlkTotal:   blkTotal,
		NdxPos:     ndxPos,
		NdxSz:      ndxSz,
		Version:    GVersion,
		CheckSum:   CheckSumBytes,
	}

	_, err := headerFd.Seek(0, os.SEEK_SET)
	if err != nil {
		return err
	}

	err = binary.Write(headerFd, binary.LittleEndian, h)
	if err != nil {
		return err
	}

	// End with error when we've written past header size,
	// which is usually one block (blockSz)
	// This could happen if someone accidentally sets Header struct
	// larger that block size (blockSz) of bytes.
	// This should normally not happen, but who knows ...
	if n, _ := headerFd.Seek(0, os.SEEK_CUR); n > GSz {
		return errors.New("Wrote beyond block boundry!")
	}

	return nil
}
