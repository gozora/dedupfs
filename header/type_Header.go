// Archive file header information. So far nothing interesting
// is going on here, except defined block size and check-sum.
package header

const (
	CheckSumBytes       = 0xDEADBEEF
	GSz           int64 = 512
	GVersion      int64 = 1
)

type Header struct {
	BlockSz    int64
	FilesCnt   int64
	BlkDeduped int64
	BlkTotal   int64
	NdxPos     int64
	NdxSz      int64
	Version    int64
	CheckSum   uint32
}

func (h Header) IsOk() bool {
	if h.CheckSum == CheckSumBytes {
		return true
	}

	return false
}
