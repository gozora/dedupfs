# dedupfs

Package creates archive containing de-duplicated content (files) and provides it 
as _fs.FS_ type which can be converted using _http.FS_ to _http.Filesystem_ 
and passed to http.FileServer to be served to end users.

## Project status
Preparing for stable

**TODOs:**
 - [ ] check if example is still valid
 - [ ] do some more testing
 - [ ] add more function descriptions

## Example
```go
package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/gozora/dedupfs"
)

const (
	archiveName   = "archive.db"
)

func create() {
	err := dedupfs.Create(archiveName, os.Args[1:])
	if err != nil {
		log.Fatal(err)
	}
}

func serve() {
	myFs, err := dedupfs.Open(archiveName)
	if err != nil {
		log.Fatal(err)
	}
	defer myFs.Destroy()

	err = http.ListenAndServe(":80", http.FileServer(http.FS(myFs)))
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	// Create archive
	create()

	// Serve content over http
	serve()
}
```

