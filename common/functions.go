// Place for functions, which don't fit anywhere better.
package common

import (
	"os"
	"strconv"
)

// Min() compares and returns smaller of x, y.
func Min(x, y int64) int64 {
	if x < y {
		return x
	}

	return y
}

func LoadVar(name string, defVal interface{}) interface{} {
	val, exists := os.LookupEnv(name)

	switch defVal.(type) {
	case string:
		if !exists {
			return defVal
		}

		return val
	case int:
		if !exists {
			return defVal
		}

		Int, _ := strconv.Atoi(val)
		return Int
	default:
		return defVal
	}
}
