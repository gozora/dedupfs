package dedupfs

import (
	"sort"
	"strings"
)

type MasterIndex map[string]FInfo

func (mi MasterIndex) SortKeys() []string {
	var keys []string
	for k := range mi {
		keys = append(keys, k)
	}

	sort.Slice(keys, func(i, j int) bool {
		return strings.Count(keys[i], "/") < strings.Count(keys[j], "/")
	})

	return keys
}
