package dedupfs

import (
	"fmt"
	"log"
	"os"
	"testing"

	"gitlab.com/gozora/dedupfs/common"
	"gitlab.com/gozora/dedupfs/printif"
)

const (
	defaultName = "test_deleteme"
)

var (
	testDirPath string = common.LoadVar("DFS_TEST_SRC_DIR",
		"/mnt/dedup/source_files").(string)
	dstDir      string = common.LoadVar("DFS_TEST_DST_DIR", ".").(string)
	testBlockSz int64  = int64(common.LoadVar("DFS_TEST_BS", 4096).(int))
	archiveName string = dstDir + "/" + defaultName + ".db"
)

func ExampleCreate() {
	err := Create("archive.db", []string{"files/*", "/usr/bin", "/bin/*"})
	if err != nil {
		log.Fatal(err)
	}
}

func deleteTestFiles() {
	os.Remove(archiveName)
	os.RemoveAll(dstDir + "/extract")
}

func TestCreate(t *testing.T) {
	GPrefixDir = "."
	GBlockSz = testBlockSz
	t.Cleanup(deleteTestFiles)

	t.Run("Create archive", func(t *testing.T) {
		log.Printf("Using block size: %v\n", GBlockSz)
		for i := 0; i < 1; i++ {
			err := Create(archiveName, []string{testDirPath})
			if err != nil {
				t.Fatalf(`Create("%v", []string{"%v"}), %v`,
					archiveName, testDirPath, err)
			}
		}
	})

	t.Run("List archive", func(t *testing.T) {
		myFs, err := Open(archiveName)
		if err != nil {
			t.Fatalf(`Open("%v"), %v`, archiveName, err)
		}
		defer myFs.Destroy()

		fmt.Println(myFs)
	})
}

func BenchmarkMain(b *testing.B) {
	printif.Gactive |= printif.OptSilent
	GBlockSz = testBlockSz
	b.Cleanup(deleteTestFiles)
	b.Logf("BenchmarkMain using blocksize: %v\n", GBlockSz)

	var err error
	success := b.Run("Creating archive", func(b *testing.B) {

		for i := 0; i < b.N; i++ {
			err = Create(archiveName, []string{testDirPath})
			if err != nil {
				b.Fatal(err)
			}
		}

		archiveSize, _ := os.Stat(archiveName)

		b.Logf("%v size %v bytes, block size %v\n",
			archiveName, archiveSize.Size(), testBlockSz)
	})

	if !success {
		b.Fatal(err)
	}

	var myFs *DedupFS
	success = b.Run("Opening archive", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			myFs, err = Open(archiveName)
			if err != nil {
				b.Fatal(err)
			}
		}
	})

	if !success {
		b.Fatal(err)
	}

	b.Run("Reading archive", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			result = myFs.GetAllFiles(Full)
		}
	})

	b.Run("Extracting archive", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			err = myFs.Extract([]string{"/"}, dstDir+"/extract")
			if err != nil {
				b.Fatal(err)
			}
		}
	})

	defer myFs.Destroy()
}
