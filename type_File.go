package dedupfs

import (
	"io"
	"io/fs"
	"os"

	"gitlab.com/gozora/dedupfs/common"
	"gitlab.com/gozora/dedupfs/header"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

type File struct {
	fs            *DedupFS
	fd            *os.File
	name          string
	fileContent   FInfo
	head          int64
	readRemains   int64
	readBlock     int64
	blockBuf      []byte
	blockBufValid bool
	stats         FileInfo
}

func (f File) Close() error {
	printIf.Debug.Println("dedupfs.File:Close()")
	return nil
}

func (f *File) Read(p []byte) (int, error) {
	printIf.Debug.Println("dedupfs.File:Read()")
	var blockSz int64 = f.fs.header.BlockSz

	// File is empty (size == 0)
	// To be compatible with standard File.Read(), we will send EOF next
	// function call after reading is done.
	if f.fileContent.Sz == 0 || f.readRemains <= 0 {
		return 0, io.EOF
	}

	if f.blockBufValid == false {
		r, err := f.fd.ReadAt(f.blockBuf,
			header.GSz+f.fileContent.Blocks[f.head]*blockSz)
		if err != nil {
			return r, err
		}

		f.blockBufValid = true
	}

	end := common.Min(blockSz, f.readRemains+f.readBlock)
	n := copy(p, f.blockBuf[f.readBlock:end])

	f.readRemains -= int64(n)
	f.readBlock += int64(n)

	if f.readBlock >= blockSz {
		f.readBlock = 0
		f.head++
		f.blockBufValid = false
	}

	return n, nil
}

func (f *File) Seek(offset int64, whence int) (int64, error) {
	printIf.Debug.Println("dedupfs.File:Seek()")
	if whence == os.SEEK_SET && offset == 0 {
		f.head = offset
		f.readBlock = 0
		f.readRemains = f.fileContent.Sz
		f.blockBufValid = false
	}

	return 0, nil
}

func (f File) Readdir(count int) ([]fs.FileInfo, error) {
	printIf.Debug.Println("dedupfs.File:Readdir()")
	return nil, nil
}

func (f File) ReadDir(count int) ([]fs.DirEntry, error) {
	printIf.Debug.Println("dedupfs.File:ReadDir()")

	// If anything but empty DirEntry is returned, client would be provided
	// with simple HTML page with links to files in respective directory.
	return []fs.DirEntry{}, nil
}

func (f File) Stat() (fs.FileInfo, error) {
	printIf.Debug.Println("dedupfs.File:Stat()")

	return f.stats, nil
}
