// Provides de-duplicated archive that is presentable
// by net/http package.
package dedupfs

import (
	"bufio"
	"crypto/sha1"
	"encoding/gob"
	"errors"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gozora/dedupfs/header"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

// Use GBlockSz bytes for archive block size. This global variable is used
// when Creating() (writing) and also when serving (reading) archive.
// GPrefixDir will be prepended when relative path is used as files parameter
// in Create().
var (
	GBlockSz   int64  = 4096
	GPrefixDir string = ""
)

type dbType struct {
	fData        FInfo // reusable buffer
	blockDeduped int64
	filesCnt     int64
	nextBlock    int64
	hashNdx      map[[20]byte]int64 // not permanently stored
	enc          *gob.Encoder
}

func writeArchive(fName string, bufferedArchiveFd *bufio.Writer,
	db *dbType) error {

	fStats, err := os.Lstat(fName)
	if err != nil {
		return err
	}

	db.fData.Sz = fStats.Size()

	// Have all map keys using same set (align relative and absolute paths)
	// as we want user to be able to choose how files/paths will be specified.
	// We will convert all relative paths (if any) to absolute.
	dbName := fName
	if !filepath.IsAbs(fName) {
		dbName = filepath.Join(GPrefixDir, fName)
	}

	dbName = convertPath(dbName)

	// We are ignoring special files like device files.
	// Symlinks are special case that is processed.
	if !fStats.Mode().IsRegular() {
		// Store information about symlink.
		// We are purposely not resolving symlinks because resolved symlinks
		// become absolute paths and later when serving archive,
		// we can wrongfully assume that file is not present in archive even
		// if it is.
		// E.g. having following setup and resolving symlinks in archive created
		// with /go/d/files directory.
		//
		// root@dev_1:/# ls -al /go/d /go/d/files/{f6,f6_l}
		// lrwxrwxrwx 1 root   root     14 Mar 10 21:15 /go/d -> /code/go/dedup
		// -rw-r--r-- 1 sodoma sodoma 1537 Feb 28 20:47 /go/d/files/f6
		// lrwxrwxrwx 1 sodoma sodoma    2 Mar 11 08:30 /go/d/files/f6_l -> f6
		//
		// would result to f6_l -> /code/go/dedup/files/f6 which would
		// be evaluated as non existent.

		if fStats.Mode()&fs.ModeSymlink != 0 {
			l, err := os.Readlink(fName)
			if err != nil {
				return err
			}

			printIf.VerboseDebug.Println("Add [l]:", dbName)
			db.fData.Link = l

			x := db.fData
			x.Blocks = x.Blocks[:x.head]

			// Buffered write to disk
			err = db.enc.Encode(MasterIndex{dbName: x})
			if err != nil {
				return err
			}

			db.filesCnt++
			db.fData.Clear()
		}

		return nil
	}

	// Now, when we are dealing with regular file, we can open it.
	printIf.VerboseDebug.Println("Add [f]:", dbName)
	f, err := os.Open(fName)
	if err != nil {
		return err
	}
	defer f.Close()
	bufferedF := bufio.NewReader(f)

	// Do actual write of files into archive.
	buf := make([]byte, GBlockSz)
	for {
		// Read() will return EOF next iteration after everything is read from
		// file. That is why we can safely check for EOF immediately after Read()
		// is called without worrying that loop iteration will be interrupted
		// prematurely.
		n, err := bufferedF.Read(buf)
		if err != nil {
			if err != io.EOF {
				return err
			}

			break
		}

		// Calculate checksum, and convert it to string to
		// make it compatible/ready for json.Marshal later.
		ckSum := sha1.Sum(buf[:n])

		// Append to checksum index if not already exist
		blkID, ok := db.hashNdx[ckSum]
		if !ok {
			db.hashNdx[ckSum] = db.nextBlock
			blkID = db.nextBlock
			db.nextBlock++

			// Write block into database
			// To have aligned archive file we will write ENTIRE buffer, even
			// if buf may contain invalid (data from previous read) data.
			n, err = bufferedArchiveFd.Write(buf)
			if err != nil {
				return err
			}
		} else {
			db.blockDeduped++
		}

		db.fData.AddBlock(blkID)
	}

	// db.fData (including fData.Blocks) is reused throughout function call to
	// avoid excessive (re)allocations.
	// db.fData.Blocks is grown if necessary (by FInfo.AddBlock) and its end is
	// marked by db.fData.head.
	// To evade writing unnecessary data from db.fData.Blocks to master index
	// file, whole structure is copied into 'x' and x.Blocks is trimmed to use
	// only first x.head members.
	// I've tried to write custom (Un)MarshalBinary() but it was way more
	// memory intensive than current approach.
	x := db.fData
	x.Blocks = x.Blocks[:x.head]

	// Buffered write to disk
	err = db.enc.Encode(MasterIndex{dbName: x})
	if err != nil {
		return err
	}

	db.filesCnt++
	db.fData.Clear()

	return nil
}

// globFiles returns expanded list of all files that should be added into
// archive. It outputs all files contained in dirs parameter recursively.
func globFiles(dirs []string) ([]string, error) {
	var files []string

	visit := func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		files = append(files, path)

		return nil
	}

	for _, dir := range dirs {
		err := filepath.WalkDir(dir, visit)
		if err != nil {
			return nil, err
		}
	}

	return files, nil
}

func convertPath(path string) string {
	// On windows we can consider ":" directory separator, this will help
	// distinguish between same named directories (paths) across different
	// disk driver (c:\, d:\ ...)
	// Replace with double forward slash ("//") to provide better pattern for
	// successive Clean(). It happened on Windows that providing only single "/"
	// did not cleaned that path correctly.
	path = strings.ReplaceAll(path, `:`, `//`)

	// Cleanup the path and replace backslash with forward slash because
	// Clean() will convert them according GOOS
	path = filepath.Clean(path)
	path = strings.ReplaceAll(path, `\`, `/`)

	// Now check if cleaned part of slice begins with "/" and prepend if not.
	if !strings.HasPrefix(path, `/`) {
		path = `/` + path
	}

	return path
}

// Create archive containing de-duplicated content set by user, files parameter,
// and store output in archiveName.
//
// Requested files to store can be absolute or relative path to desired file and
// must exists. If file specified does not exist, error will be returned by
// function and execution will abort.
// If path is specified as relative, current working directory (CWD)
// will be prefixed to each file. If files parameter contains relative path
// and GPrefixDir is not empty string, GPrefixDir and current working directory
// (CWD) will be prefixed to each file.
//
// When files parameter contains patterns, these patterns will be expanded.
// The syntax of patterns is the same as in path.Match
// (https://pkg.go.dev/path/filepath#Match).
// The pattern may describe hierarchical names such as usr/*/bin/ed.
//
// The pattern syntax is:
//
//	pattern:
//		{ term }
//	term:
//		'*'         matches any sequence of non-Separator characters
//		'?'         matches any single non-Separator character
//		'[' [ '^' ] { character-range } ']'
//		            character class (must be non-empty)
//		c           matches character c (c != '*', '?', '\\', '[')
//		'\\' c      matches character c
//
//	character-range:
//		c           matches character c (c != '\\', '-', ']')
//		'\\' c      matches character c
//		lo '-' hi   matches character c for lo <= c <= hi
func Create(archiveName string, files []string) error {
	if GBlockSz < 512 {
		return errors.New("Block size too small. Must be larger equal" +
			" to 512 bytes.")
	}

	printIf.NotSilent.Printf("Creating archive %v with block size %v bytes.\n",
		archiveName, GBlockSz)

	// Create new archive file. Content of old file (if any)
	// will be removed.
	archiveFd, err := os.OpenFile(archiveName,
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}

	// Cleanup archive file when error occurred.
	defer func() {
		if err != nil {
			printIf.Debug.Printf("dedupfs: Create(): Removing file: %v\n",
				archiveName)
			os.Remove(archiveName)
		}
	}()
	defer archiveFd.Close()

	// Use temporary file for storage of master index.
	masterFd, err := os.CreateTemp("", "_dedupToGo.*.ndx")
	if err != nil {
		return err
	}
	defer os.Remove(masterFd.Name())
	defer masterFd.Close()

	bufferedMasterFd := bufio.NewWriter(masterFd)
	db := &dbType{
		hashNdx:   make(map[[20]byte]int64),
		nextBlock: 0, // first block reserved for header
		enc:       gob.NewEncoder(bufferedMasterFd),
	}

	// Allocate some blocks in advance. When out of space FInfo.AddBlock,
	// will allocate additional memory, similar to append().
	db.fData.Blocks = make([]int64, preallocateBlocks)

	// Move file cursor to position where data will be written.
	// First block is intentionally skipped and will contain header information.
	_, err = archiveFd.Seek(header.GSz, os.SEEK_SET)
	if err != nil {
		return err
	}

	dirs, err := globFiles(files)
	if err != nil {
		return err
	}

	// Cleanup and unify prefix specified by user (--prefix-dir).
	// Since user can input virtually any string, we do out best to cleanup the
	// input.
	// prefix-dir will be converted to always begin with "/" so we have always
	// absolute paths to files in archive.
	// (see comment in dedupfs.DedupFS.Open())
	// If prefix-dir contains ";" or drive specification "c:\\" it will be
	// removed as well.
	// This function also converts windows specification of prefix to Linux
	// notation.
	GPrefixDir = convertPath(GPrefixDir)

	bufferedArchiveFd := bufio.NewWriter(archiveFd)
	for _, f := range dirs {
		err = writeArchive(f, bufferedArchiveFd, db)
		if err != nil {
			return err
		}
	}

	if err = bufferedMasterFd.Flush(); err != nil {
		return err
	}

	if err = bufferedArchiveFd.Flush(); err != nil {
		return err
	}

	// Append master index to the end of archive file
	ndxSz, err := masterFd.Seek(0, os.SEEK_CUR)
	if err != nil {
		return err
	}

	ndxPos, err := archiveFd.Seek(0, os.SEEK_CUR)
	if err != nil {
		return err
	}

	_, err = masterFd.Seek(0, os.SEEK_SET)
	if err != nil {
		return err
	}

	printIf.Verbose.Println("Finishing storage of master index.")
	bufferedReadMasterNdx := bufio.NewReader(masterFd)
	n, err := io.Copy(bufferedArchiveFd, bufferedReadMasterNdx)
	if err != nil {
		return err
	}

	if n != ndxSz {
		return errors.New("Failed to write master index.")
	}

	if err = bufferedArchiveFd.Flush(); err != nil {
		return err
	}

	// Write header
	blkTotal := db.blockDeduped + db.nextBlock
	err = header.Write(archiveFd, GBlockSz, db.filesCnt, db.blockDeduped,
		blkTotal, ndxPos, ndxSz)
	if err != nil {
		return err
	}

	printIf.NotSilent.Printf("Deduplicated %v of total %v blocks in %v files.\n",
		db.blockDeduped, blkTotal, db.filesCnt)

	return nil
}
