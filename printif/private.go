package printif

import "fmt"

// Show message only if at least one of user specified options is set.
func (o Opts) println(a ...interface{}) {
	if o.Active&Gactive != 0 {
		o.printWithPrefix(a...)
	}
}

// Show message only if NON of specified options is set by user.
func (o Opts) invertPrintln(a ...interface{}) {
	if !o.isActive() {
		o.printWithPrefix(a...)
	}
}

func (o Opts) printWithPrefix(a ...interface{}) {
	rv := a
	if o.Prefix != "" {
		var t = make([]interface{}, len(a)+1)

		a = append(a, "")
		copy(t[1:], a[0:])
		t[0] = o.Prefix

		rv = t

	}

	fmt.Println(rv...)
}

func (o Opts) printf(format string, a ...interface{}) {
	if o.Active&Gactive != 0 {
		if o.Prefix != "" {
			format = fmt.Sprintf("%s %s", o.Prefix, format)
		}
		fmt.Printf(format, a...)
	}
}

func (o Opts) invertPrintf(format string, a ...interface{}) {
	if !o.isActive() {
		if o.Prefix != "" {
			format = fmt.Sprintf("%s %s", o.Prefix, format)
		}
		fmt.Printf(format, a...)
	}
}

// Returns true if at least one of parameters in o.opts is active in Gactive.
// Used by invert*().
func (o Opts) isActive() bool {
	for i := 0; i < optCnt; i++ {
		if o.Active&(1<<i) != 0 {
			// Is nth bit set by user active?
			if Gactive&(1<<i) != 0 {
				return true
			}
		}
	}

	return false
}
