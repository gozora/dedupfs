package printif

func (o Opts) Println(a ...interface{}) {
	switch o.Invert {
	case true:
		o.invertPrintln(a...)
	default:
		o.println(a...)
	}
}

func (o Opts) Printf(format string, a ...interface{}) {
	switch o.Invert {
	case true:
		o.invertPrintf(format, a...)
	default:
		o.printf(format, a...)
	}
}
