// Package printif serves for conditional information printing.
package printif

type Opts struct {
	Active int64  // When should user see the output
	Invert bool   // Negate Active
	Prefix string // Prefix that should be printed before the message
}

const (
	OptSilent int64 = 1 << iota
	OptDebug
	OptVerbose
	optCnt int = 3
)

var (
	Gactive      int64
	NotSilent    Opts
	Debug        Opts
	Verbose      Opts
	VerboseDebug Opts
)

func init() {
	// Gactive |= int64(OptAlways)

	NotSilent = Opts{OptSilent, true, ""}
	Debug = Opts{OptDebug, false, "DEBUG:"}
	Verbose = Opts{OptVerbose, false, ""}
	VerboseDebug = Opts{OptVerbose | OptDebug, false, ""}
}
