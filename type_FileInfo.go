package dedupfs

import (
	"io/fs"
	"time"
)

type FileInfo struct {
	de      *DentryT
	name    string
	size    int64
	mode    fs.FileMode
	modTime time.Time
	isDir   bool
	sys     interface{}
}

func (fi FileInfo) GetFileContent() FInfo {
	return fi.de.fileContent
}

func (fi FileInfo) Name() string {
	return fi.name
}

func (fi FileInfo) Size() int64 {
	return fi.size
}

func (fi FileInfo) Mode() fs.FileMode {
	return fi.mode
}

func (fi FileInfo) ModTime() time.Time {
	return fi.modTime
}

func (fi FileInfo) IsDir() bool {
	return fi.isDir
}

func (fi FileInfo) Sys() interface{} {
	return nil
}
