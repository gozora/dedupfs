package dedupfs

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/gozora/dedupfs/header"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

// Main structure holding archive index and open file descriptor and some
// other properties.
type DedupFS struct {
	archiveFd  *os.File    // Main output goes here
	masterNdx  MasterIndex // File content information (blocks, size, ...)
	inodes     *DentryT
	header     *header.Header // Archive meta information
	maxLinks   int            // How much depth is allowed in links
	Extracting bool           // Links are handled differently when extracting
}

func (f DedupFS) Destroy() {
	f.archiveFd.Close()
}

// Dump() dumps/converts binary representation of master index stored in binary
// file into JSON.
func (f DedupFS) Dump() string {
	buf, err := json.Marshal(f.masterNdx)
	if err != nil {
		printIf.NotSilent.Println(err)
		return ""
	}

	return string(buf)
}

// Extract files from archive without explicitly serving over HTTP.
func (f *DedupFS) Extract(files []string, dstDir string) error {
	printIf.Debug.Println("dedupfs:DedupFS: Extract()")

	matches, err := f.globFiles(files)
	if err != nil {
		return err
	}

	readBuf := make([]byte, GBlockSz)
	// Proceed with file extraction from archive.
	m := f.inodes.getMatches(matches)
	cnt := 0
	for _, entry := range m {
		fileName := entry.fullPath()

		printIf.VerboseDebug.Println("Extracting file:", fileName)

		// Load file from archive
		archiveFile, err := f.Open(fileName)
		if err != nil {
			return err
		}
		bufferedArchiveFile := bufio.NewReader(archiveFile)

		// Create directory structure
		err = os.MkdirAll(dstDir+filepath.Dir(fileName), 0777)
		if err != nil {
			return err
		}

		// Create link and skip to next loop iteration.
		if entry.isLink() {
			os.Symlink(entry.link.dstName, dstDir+fileName)
			cnt++
			continue
		}

		// Create destination file.
		dstFileFd, err := os.OpenFile(dstDir+fileName,
			os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0777)
		if err != nil {
			return err
		}
		defer dstFileFd.Close()

		bufferedDstFileFd := bufio.NewWriter(dstFileFd)
		defer bufferedDstFileFd.Flush()

		// Create file.
		for {
			r, err := bufferedArchiveFile.Read(readBuf)
			if err != nil {
				if err != io.EOF {
					return err
				}

				break
			}

			w, err := bufferedDstFileFd.Write(readBuf[:r])
			if err != nil {
				return err
			}

			// We failed to write same amount of bytes that was read.
			if r != w {
				return fmt.Errorf("Wrote only %v bytes of %v\n", w, r)
			}
		}
		cnt++
	}

	cdir := dstDir
	if cdir == "./" {
		cdir, err = os.Getwd()
		if err != nil {
			return err
		}
	}

	printIf.NotSilent.Printf("Extracted %v files into %v directory.\n",
		cnt, cdir)

	return nil
}

// Returns array of strings representing ALL files present in archive.
// outputType can be formatted as follows:
//	Full:       [F] /home/links/f1
//	            [L] /home/z1 -> dir
//	            [D] /home/links
//	NamesOnly:  /home/links/f1
//	            /home/z1
//	NoType:     /home/links/f1
//	            /home/z1 -> dir
//	            /home/links
func (f DedupFS) GetAllFiles(outputType int) []string {
	return f.inodes.list(true, false, outputType)
}

func (f *DedupFS) Open(name string) (fs.File, error) {
	// http.FS() conversion removes leading "/" (if used)
	// but DedupFS is working with absolute paths so we just add it back.
	if !strings.HasPrefix(name, `/`) {
		name = "/" + name
	}

	printIf.Debug.Printf("dedupfs:DedupFS: Open(): %v\n", name)

	fStats, err := f.Stat(name)
	if err != nil {
		return nil, err
	}

	// Throw error when file requested over HTTP is a symbolic link without
	// any destination. (fStat should contain resolved link at this stage)
	if f.Extracting == false && fStats.(FileInfo).de.isLink() {
		pErr := os.PathError{
			Op:   "DedupFs:Open():",
			Path: name,
			Err:  fs.ErrNotExist,
		}

		return nil, &pErr
	}

	serveFile := File{
		name:        fStats.Name(),
		fs:          f,
		fd:          f.archiveFd,
		fileContent: fStats.(FileInfo).GetFileContent(),
		blockBuf:    make([]byte, f.header.BlockSz),
		stats:       fStats.(FileInfo),
	}
	serveFile.Seek(0, os.SEEK_SET)

	return &serveFile, nil
}

// Function used when extract workflow is called and wildcards are used in
// file/directory paths.
func (f *DedupFS) ReadDir(name string) ([]fs.DirEntry, error) {
	printIf.Debug.Println("DedupFS.ReadDir()")

	if !strings.HasPrefix(name, `/`) {
		name = `/` + name
	}

	dir, err := f.inodes.getDentry(name, true)
	if err != nil {
		return nil, err
	}

	rv := make([]fs.DirEntry, len(dir.child))
	for ndx, entry := range dir.child {
		rv[ndx] = fs.FileInfoToDirEntry(FileInfo{
			name:    entry.name,
			size:    0,
			mode:    0644,
			modTime: time.Date(1970, time.January, 1, 1, 0, 0, 0, time.UTC),
			isDir:   entry.isDir(),
		})
	}

	return rv, nil
}

func (f DedupFS) Stat(name string) (fs.FileInfo, error) {
	printIf.Debug.Println("DedupFS.Stat()")

	followLink := true
	if f.Extracting {
		followLink = false
	}

	de, err := f.inodes.root.getDentry(name, followLink)
	if err != nil {
		pErr := os.PathError{
			Op:   "DedupFs:Stat():",
			Path: name,
			Err:  fs.ErrNotExist,
		}

		return nil, &pErr
	}

	rv := FileInfo{
		de:      de,
		name:    de.name,
		size:    de.size(),
		mode:    0644,
		modTime: time.Date(1970, time.January, 1, 1, 0, 0, 0, time.UTC),
		isDir:   de.isDir(),
	}

	return rv, nil
}

func (f DedupFS) String() string {
	return strings.Join(f.inodes.list(true, false, Full), "\n")
}

// Returns globed slice of strings containing files matching pattern in
// files parameter. The syntax of patterns is the same as in path.Match.
// The pattern may describe hierarchical names such as usr/*/bin/ed.
func (f *DedupFS) globFiles(files []string) ([]string, error) {
	var matches []string

	for _, v := range files {
		vClean := path.Clean(v)

		match, err := fs.Glob(f, vClean)
		if err != nil {
			return nil, err
		}

		matches = append(matches, match...)
	}

	// Fix that allows using wildcards in first part of path (/1/2/3/4)
	// when using pattern like `/l*` fs.Glob function splits pattern into dir and
	// file variable which are later merged. Using wildcard in first part of
	// path resulted to directory be empty (""), which creates relative paths
	// later when joining.
	// This fix checks matches and prepares them to be always absolute.
	var prefixedMatches []string
	for _, v := range matches {
		if !strings.HasPrefix(v, `/`) {
			prefixedMatches = append(prefixedMatches, `/`+v)
		} else {
			prefixedMatches = append(prefixedMatches, v)
		}
	}

	// Handle error here because error handled in ReadDir() (which is called by
	// fs.Glob()) is discarded (Go 1.17.3) in io/fs/glob.go:93
	if len(prefixedMatches) == 0 {
		err := os.PathError{
			Op:   "DedupFs:globFiles():",
			Path: strings.Join(files, ", "),
			Err:  fs.ErrNotExist,
		}

		return nil, &err
	}

	return prefixedMatches, nil
}
