package dedupfs

const preallocateBlocks = 4096

type FInfo struct {
	Link   string
	Blocks []int64
	Sz     int64
	head   int
}

func (fi *FInfo) AddBlock(blk int64) {
	fi.Blocks[fi.head] = blk
	fi.head++

	if fi.head == cap(fi.Blocks) {
		t := make([]int64, fi.head*2)

		copy(t, fi.Blocks)

		fi.Blocks = t
	}
}

func (fi *FInfo) Clear() {
	for i := 0; i < fi.head; i++ {
		fi.Blocks[i] = 0
	}

	fi.Link = ""
	fi.head = 0
	fi.Sz = 0
}

func (fi FInfo) IsLink() bool {
	if len(fi.Link) > 0 {
		return true
	}

	return false
}
