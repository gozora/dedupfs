#!/usr/bin/env bash

pushd /home/sodoma/nfs/nfs-server/go/dedupfs 1>/dev/null || exit 1

GOBG_TAG=$(git log --pretty=format:"%h" -1)
export GOBG_TAG

go test -bench=BenchmarkMain -count 3  -run ^$ -benchmem | tee /dev/tty | \
parseOutput >> reports/benchmark_main.log
