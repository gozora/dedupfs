#!/usr/bin/env bash

graph_dir="reports/graph"
input_data="reports/benchmark_main.log"

GOBG_ANN="true"
export GOBG_ANN

for filter in "openDuration" "readDuration" "createDuration" \
"openMem" "readMem" "createMem"; do
	# shellcheck disable=SC2034
	GOBG_FILTER=$filter
	drawChart < $input_data > $graph_dir/$filter.png
done
