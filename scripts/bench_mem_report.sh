#!/usr/bin/env bash

pushd /home/sodoma/nfs/nfs-server/go/dedupfs 1>/dev/null || exit 1

suffix=$(date +"%d%m%y_%H%M%S.%3N")
commit=$(git rev-parse --short HEAD)

export DFS_TEST_DST_DIR="/mnt/dedup"
export DFS_TEST_SRC_DIR="/mnt/dedup/source_files"

for bs in 512 4096 8192; do
	export DFS_TEST_BS=$bs 
	go test -bench=BenchmarkMain -benchmem -run ^$ \
		-benchtime 1x -count 1 \
	 	-memprofile reports/mem_"$commit"_${DFS_TEST_BS}_"$suffix".prof 
done | tee reports/report_"$commit"_"$suffix".log

echo "\$DFS_TEST_DST_DIR="$DFS_TEST_DST_DIR >> reports/report_"$commit"_"$suffix".log
echo "\$DFS_TEST_SRC_DIR="$DFS_TEST_SRC_DIR >> reports/report_"$commit"_"$suffix".log

echo "Log written to reports/report_${commit}_$suffix.log"
