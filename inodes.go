package dedupfs

import (
	"errors"
	"fmt"
	"path"
	"sort"
	"strings"

	printIf "gitlab.com/gozora/dedupfs/printif"
)

const (
	FileType = iota
	DirectoryType
	SymLinkType
)

const (
	Full = iota
	NamesOnly
	NoType
)

var (
	errDentryNotFound = errors.New("Entry does not exist.")
	errDentryNotLink  = errors.New("Not a symlink.")
	errDentryMaxLink  = errors.New("Link too deep.")

	MaxLinkDepth = 8
)

type dentryError struct {
	err      error
	atFunc   string
	atDentry string
	caller   string
}

func (de dentryError) String() string {
	switch de.err {
	case errDentryNotFound:
		return fmt.Sprintf("%v: %v: %v", de.atFunc, de.caller, de.err)
	default:
		return fmt.Sprintf("%v: %v: %v", de.atFunc, de.atDentry, de.err)
	}
}

func (de dentryError) Error() string {
	return de.String()
}

type linkT struct {
	dstName string
	dst     *DentryT
}

type DentryT struct {
	name        string
	root        *DentryT
	parent      *DentryT
	child       []*DentryT
	fileContent FInfo
	link        *linkT
	eType       uint
}

func (de *DentryT) addChild(child *DentryT) *DentryT {
	de.child = append(de.child, child)

	return child
}

func (de *DentryT) createLinksCache() *dentryError {
	if de.isLink() {
		l, err := de.root.resolveLink(de.fullPath(), false, MaxLinkDepth, de)
		if err != nil {
			return err
		}

		d, err := de.root.getDentry(l, true)
		if err != nil {
			return err
		}

		de.link.dst = d
	}

	if len(de.child) == 0 {
		return nil
	}

	for _, val := range de.child {
		err := val.createLinksCache()

		if err != nil {
			err.atFunc = "CreateLinksCache()"

			switch err.err {
			case errDentryMaxLink, errDentryNotFound:
				err.caller = val.fullPath()

				printIf.NotSilent.Println(err)
			}
		}
	}

	return nil
}

func (de *DentryT) find(pathStr string, getPath bool,
	followLink bool) (*DentryT, *dentryError) {
	// pathStr == ""; When GetDentry("/") || GetDentry("") is requested, because
	// strings.Split() strips separator.
	// pathStr == "/"; should happen only when de.find() is called manually.
	if pathStr == "" || pathStr == "/" {
		return de, nil
	}

	for _, child := range de.child {
		if child.name == pathStr {
			if child.isCached() && followLink {
				return child.link.dst, nil
			} else {
				return child, nil
			}
		}
	}

	err := &dentryError{
		err:    errDentryNotFound,
		atFunc: "find()",
	}

	if getPath {
		if de.isRoot() {
			err.atDentry = "/" + pathStr
		} else {
			err.atDentry = de.fullPath() + "/" + pathStr
		}
	}

	return nil, err
}

func (de *DentryT) follow() *DentryT {
	if de.link != nil && de.link.dst != nil {
		return de.link.dst.follow()
	}

	return de
}

// Returns full path to de.
// Function was primarly created to avoid storing long strings inside structure,
// that are not always needed.
// Function DOES NOT resolve links.
func (de *DentryT) fullPath() string {
	depth := de.getDepth()

	// Special case when de.name == "/"
	if depth == 0 {
		return "/"
	}

	// Accommodate one more member for "/" at start of the array
	path := make([]string, depth+1)
	head := de

	// Fill array from the end because we are currently at the last dentry.
	// First element of array will remain empty to represent root ("/"").
	for i := depth; i > 0; i-- {
		path[i] = head.name
		head = head.parent
	}

	return strings.Join(path, "/")
}

func (de DentryT) getChildDentries() []*DentryT {
	var rv []*DentryT

	if de.isDir() {
		for _, entry := range de.child {
			if !entry.isDir() {
				rv = append(rv, entry)
			}

			rv = append(rv, entry.getChildDentries()...)
		}
	}

	return rv
}

// Returns dentry specified by string pathStr. pathStr can be both directory
// and file.
// Start of search tree is specified by de.
func (de *DentryT) getDentry(pathStr string,
	followLink bool) (*DentryT, *dentryError) {
	if !strings.HasPrefix(pathStr, "/") {
		pathStr = "/" + pathStr
	}
	pathStr = path.Clean(pathStr)

	pathSeg := strings.Split(pathStr, "/")

	head := de
	var err *dentryError
	for i := 1; i < len(pathSeg); i++ {
		head, err = head.find(pathSeg[i], true, followLink)
		if err != nil {
			return nil, err
		}
	}

	return head, nil
}

func (de *DentryT) getDepth() (i int) {
	head := de
	for !head.isRoot() {
		i++
		head = head.parent
	}

	return
}

func (de DentryT) getMatches(matches []string) []*DentryT {
	var rv []*DentryT

	for _, match := range matches {
		entry, _ := de.getDentry(match, false)

		if entry.isDir() {
			rv = append(rv, entry.getChildDentries()...)
		} else {
			rv = append(rv, entry)
		}
	}

	return rv
}

func (de DentryT) getSortedChildren() []*DentryT {
	cnt := len(de.child)
	m := make(map[string]*DentryT)
	fileNames := make([]string, cnt)

	for ndx, entry := range de.child {
		fileNames[ndx] = entry.name
		m[entry.name] = entry
	}

	sort.Strings(fileNames)

	rv := make([]*DentryT, cnt)
	for ndx, fileName := range fileNames {
		rv[ndx] = m[fileName]
	}

	return rv
}

func (de DentryT) getType() string {
	switch de.eType {
	case FileType:
		return "[F]"
	case DirectoryType:
		return "[D]"
	case SymLinkType:
		return "[L]"
	default:
		return ""
	}
}

func (de DentryT) info(outputType int) string {
	var dstLnk string
	if de.isLink() {
		dstLnk = " -> " + de.link.dstName
	}

	switch outputType {
	case Full:
		return fmt.Sprintf("%v %v%v", de.getType(), de.fullPath(), dstLnk)
	case NamesOnly:
		return fmt.Sprintf("%v", de.fullPath())
	case NoType:
		return fmt.Sprintf("%v%v", de.fullPath(), dstLnk)
	default:
		return ""
	}
}

func (de *DentryT) isCached() bool {
	if de.link != nil && de.link.dst != nil {
		return true
	}

	return false
}

func (de DentryT) isDir() bool {
	if de.eType == DirectoryType {
		return true
	}

	return false
}

func (de DentryT) isLink() bool {
	if de.link != nil {
		return true
	}

	return false
}

func (de *DentryT) isRoot() bool {
	if de == de.root {
		return true
	}

	return false
}

func (de DentryT) list(recurse, includeDir bool, outputType int) []string {
	var rv []string

	if de.isDir() {
		for _, entry := range de.getSortedChildren() {
			if !entry.isDir() || includeDir {
				rv = append(rv, entry.info(outputType))
			}

			if recurse {
				rv = append(rv, entry.list(recurse, includeDir, outputType)...)
			}
		}
	}

	return rv
}

func (de *DentryT) resolveLink(pathStr string,
	followLink bool, MaxLinkDepth int, caller *DentryT) (string, *dentryError) {
	p := path.Clean(pathStr)

	if MaxLinkDepth == 0 {
		return "", &dentryError{
			atFunc:   "resolveLink()",
			err:      errDentryMaxLink,
			atDentry: caller.fullPath(),
		}
	}

	head := de
	var err *dentryError

	pathSeg := strings.Split(p, "/")
	pathSegLen := len(pathSeg)

	p = ""
	for i := 1; i < pathSegLen; i++ {
		head, err = head.find(pathSeg[i], true, followLink)
		if err != nil {
			return "", err
		}

		if head.isLink() {
			var app string
			// Copy unprocessed dentries from pathStr for later processing.
			if i != pathSegLen-1 {
				app = strings.Join(pathSeg[i+1:], "/")
			}

			if path.IsAbs(head.link.dstName) {
				p = path.Join(head.link.dstName, app)
			} else {
				p = path.Join("/", p, head.link.dstName, app)
			}

			MaxLinkDepth--
			dstPath, err := de.root.resolveLink(p, followLink, MaxLinkDepth,
				caller)

			return dstPath, err
		} else {
			p += "/" + pathSeg[i]
		}
	}

	return p, nil
}

func (de *DentryT) setName(name string) {
	de.name = name
}

func (de *DentryT) setRoot(root *DentryT) {
	de.root = root
}

func (de *DentryT) setType(eType uint) {
	de.eType = eType
}

func (de *DentryT) size() int64 {
	if de.eType == DirectoryType {
		return 0
	}

	return de.fileContent.Sz
}

func (de *DentryT) updateSingleLinkCache() *dentryError {
	if de.isLink() {
		l, err := de.root.resolveLink(de.fullPath(), false, MaxLinkDepth, de)
		if err != nil {
			return err
		}

		d, err := de.root.getDentry(l, true)
		if err != nil {
			return err
		}

		de.link.dst = d
		return nil
	}

	return &dentryError{err: errDentryNotLink}
}

func CreateInodes(index MasterIndex, root *DentryT) error {
	var deType uint

	for _, filePath := range index.SortKeys() {
		fNfo := index[filePath]
		pathSeg := strings.Split(filePath, `/`)
		pathSegLen := len(pathSeg)

		head := root
		for i := 1; i < pathSegLen; i++ {
			// Last path fragment is file.
			if i == pathSegLen-1 {
				deType = FileType

				if fNfo.IsLink() {
					deType = SymLinkType
				}
			} else {
				deType = DirectoryType
			}

			de, err := head.find(pathSeg[i], false, false)

			// Create new entry since it don't exist in current directory (CWD).
			if err != nil && err.err == errDentryNotFound {
				newChild := &DentryT{
					name:        pathSeg[i],
					parent:      head,
					eType:       deType,
					fileContent: fNfo,
					root:        root,
				}

				if newChild.eType == SymLinkType {
					lnk := new(linkT)
					lnk.dstName = fNfo.Link

					newChild.link = lnk
				}

				head = head.addChild(newChild)
			} else {
				head = de
			}

		}
	}

	root.createLinksCache()

	return nil
}
