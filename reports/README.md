# Tests

## Environmet variables
```bash
export DFS_TEST_DST_DIR="/mnt/dedup"
export DFS_TEST_SRC_DIR="/mnt/dedup/extract"
export DFS_TEST_BS="4096"
```

## Tests

### Standard test (without benchmark)
```bash
$ go test
```

### Run selected tests (-run \<regex\>) and create memory profile
```bash
$ go test -run TestString -memprofile mem.prof
```

## Read memory profile
```bash
$ go tool pprof mem.prof
```

## Benchmarks, tests and reports

### Measurement (reference) benchmark with memory profiles
```bash
./scripts/bench_mem_report.sh
```

### Create basic report containing duration and memory utilization
```bash
./scripts/bench_main.sh
```

### Update memory and duration graphs (in reports/graph)
```bash
./scripts/update_graphs.sh
```

### Standard benchmark (run all benchmark and tests)
```bash
$ go test -bench .
```

### Standard benchmark (run only benchmark)
```bash
$ go test -bench . -run ^$
```

### Run specific benchmark without any tests
```bash
$ go test -bench=BenchmarkMain -run ^$ -benchmem
```
