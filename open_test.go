package dedupfs

import (
	"log"
	"net/http"
	"testing"

	"gitlab.com/gozora/dedupfs/printif"
)

func ExampleOpen() {
	myFs, err := Open("archive.db")
	if err != nil {
		log.Fatal(err)
	}
	defer myFs.Destroy()

	err = http.ListenAndServe(":80", http.FileServer(http.FS(myFs)))
	if err != nil {
		log.Fatal(err)
	}
}

var result []string

func BenchmarkReadLarge(b *testing.B) {
	printif.Gactive |= printif.OptSilent

	var myFs *DedupFS
	var err error

	success := b.Run("Opening archive", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			myFs, err = Open("/mnt/dedup/archive.db")
			if err != nil {
				b.Fatal(err)
			}
		}
	})

	if success {
		b.Run("Reading archive", func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				result = myFs.GetAllFiles(Full)
			}
		})

		defer myFs.Destroy()
	}
}
