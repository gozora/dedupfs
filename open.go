package dedupfs

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"errors"
	"io"
	"os"

	"gitlab.com/gozora/dedupfs/header"
	printIf "gitlab.com/gozora/dedupfs/printif"
)

func Open(archiveName string) (*DedupFS, error) {
	var err error
	fs := new(DedupFS)
	fs.maxLinks = 8

	fs.archiveFd, err = os.OpenFile(archiveName, os.O_RDONLY, 0400)
	if err != nil {
		return nil, err
	}

	// Load header
	fs.header, err = header.Read(fs.archiveFd)
	if err != nil {
		return nil, err
	}

	// Check if checksum read from file corresponds to one defined in
	// header/Header.go
	if !fs.header.IsOk() {
		return nil, errors.New("Incorrect archive header checksum.")
	}

	printIf.Verbose.Println("Loading master index...")

	_, err = fs.archiveFd.Seek(fs.header.NdxPos, os.SEEK_SET)
	if err != nil {
		return nil, err
	}

	bufferedMasterFd := bufio.NewReader(fs.archiveFd)
	buf := make([]byte, fs.header.NdxSz)
	n, err := bufferedMasterFd.Read(buf)
	if err != nil {
		return nil, err
	}

	if int64(n) != fs.header.NdxSz {
		return nil, errors.New("Failed to load master index.")
	}

	dec := gob.NewDecoder(bytes.NewBuffer(buf))
	for {
		err := dec.Decode(&fs.masterNdx)
		if err != nil {
			if err != io.EOF {
				return nil, err
			}

			break
		}
	}

	var root DentryT
	root.setType(DirectoryType)
	root.setName("/")
	root.setRoot(&root)

	err = CreateInodes(fs.masterNdx, &root)
	if err != nil {
		return nil, err
	}

	fs.inodes = &root
	fs.masterNdx = nil

	return fs, nil
}
